package com.mybatisplus.core.controller;




import com.mybatisplus.core.bean.User;
import com.mybatisplus.core.dao.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author: zhangyx
 * @Date: 2020/8/9:14:32
 * @location: home
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    private UserMapper userMapper;

    @GetMapping("/test")
    public void test(){
        System.out.println(("----- selectAll method test ------"));
        List<User> userList = userMapper.selectList(null);
        userList.forEach(System.out::println);
    }

    @GetMapping("/testinsert")
    public void testinsert(){
        System.out.println(("----- insert test ------"));
        User user = new User();
        user.setAge(10);
        user.setName("小张2");
        int insert = userMapper.insert(user);
        System.out.println(insert);
    }

    @GetMapping("/testVersion")
    public void testVersion(){
        User user = userMapper.selectById("142e9102db9fb61c64d3f7c160106870");
        user.setName("小张v2");
        userMapper.updateById(user);
    }

}
