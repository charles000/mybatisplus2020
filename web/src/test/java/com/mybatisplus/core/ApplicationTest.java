package com.mybatisplus.core;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mybatisplus.core.bean.User;
import com.mybatisplus.core.dao.UserMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * @Author: zhangyx
 * @Date: 2020/9/9:20:17
 * @location: home
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ApplicationTest {

    @Autowired
    private UserMapper userMapper;

    /*
     * 批量查询
     * */
    @Test
    public void test1(){

        List<User> users = userMapper.selectBatchIds(Arrays.asList(1,2,3));
        users.forEach(System.out::println);
    }

    /*
     * mapper查询
     * */
    @Test
    public void test2(){
        HashMap<String, Object> objectObjectHashMap = new HashMap<>();
        objectObjectHashMap.put("name","小张2");
        List<User> users = userMapper.selectByMap(objectObjectHashMap);
        users.forEach(System.out::println);

    }

    /*
     * 分页查询
     * */
    @Test
    public void test3(){
        Page<User> userPage = new Page<>(1,5);
        Page<User> userPage1 = userMapper.selectPage(userPage, null);
        List<User> records = userPage.getRecords();
        records.forEach(System.out::println);
    }
}
