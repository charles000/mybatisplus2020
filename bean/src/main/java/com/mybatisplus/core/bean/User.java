package com.mybatisplus.core.bean;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.util.Date;

/**
 * @Author: zhangyx
 * @Date: 2020/8/9:14:16
 * @location: home
 */
@Data
public class User {
    @TableId(type= IdType.ASSIGN_UUID)
    private String id;
    private String name;
    private Integer age;
    private String email;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE,update = "now()")
    private Date updateTime;

    //声明它为乐观锁字段
    @Version
    private Integer version;
}
