package com.mybatisplus.core.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mybatisplus.core.bean.User;

/**
 * @Author: zhangyx
 * @Date: 2020/8/9:14:27
 * @location: home
 */
public interface UserMapper extends BaseMapper<User> {

}
